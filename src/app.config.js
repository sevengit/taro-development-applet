export default {
  pages: [
    'pages/index/index',
    'pages/click/index',
    'pages/order/index',
    'pages/searchPage/index',
    'pages/content/index',
  ],
  window: {
    backgroundTextStyle: 'light',
    navigationBarBackgroundColor: '#fff',
    navigationBarTitleText: 'XX',
    navigationBarTextStyle: 'black',

  },
  tabBar: {
    list: [
      {
      'iconPath': 'resource/icon/index.png',
      pagePath: 'pages/index/index',
      text: '首页'
      },
      {
      'iconPath': 'resource/icon/click.png',
      pagePath: 'pages/click/index',
      text: '点单'
      },
      {
        'iconPath': 'resource/icon/click.png',
        pagePath: 'pages/order/index',
        text: '订单'
      }
    ],
      'color': '#000',
      'selectedColor': '#56abe4',
      'backgroundColor': '#fff',
      'borderStyle': 'white'
  }
}
