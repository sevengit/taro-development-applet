import Vue from 'vue'
import './app.less'
import store from './store'
import 'taro-ui/dist/style/index.scss'

import api from './api/api.js'//api接口
import http from './api/http.js'//request请求
import config from './api/config.js'
global.http = http//全局注入request请求
global.api = api//全局注入api接口
global.config = config // 公用配置

const App = new Vue({
  onLaunch(){
    console.log("初始化完成")
  },
  onShow (options) {
  },
  store,
  render(h) {
    // this.$slots.default 是将要会渲染的页面
    return h('block', this.$slots.default)
  }
})

export default App
