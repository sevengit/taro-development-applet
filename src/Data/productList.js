export const productList = [
  { title: '最新' ,
    product:[
      {id:'0001',name:'抹茶芝士',count:0,price:15,des:'醇香芝士与清新抹茶的完美碰撞，尝一口，唇齿留香'},
      {id:'0002',name:'新绿豆乳茶',count:0,price:18,des:'醇香芝士与清新抹茶的完美碰撞，尝一口，唇齿留香'},
      {id:'0003',name:'醉意山谷茶',count:0,price:25,des:'醇香芝士与清新抹茶的完美碰撞，尝一口，唇齿留香'},
      {id:'0004',name:'茶颜悦色',count:0,price:15,des:'醇香芝士与清新抹茶的完美碰撞，尝一口，唇齿留香'},
      {id:'0005',name:'奥利奥芝士奶盖',count:0,price:18,des:'醇香芝士与清新抹茶的完美碰撞，尝一口，唇齿留香'},
      {id:'0006',name:'芝士红茶',count:0,price:25,des:'醇香芝士与清新抹茶的完美碰撞，尝一口，唇齿留香'},
      {id:'0007',name:'敦煌画意红茶',count:0,price:15,des:'醇香芝士与清新抹茶的完美碰撞，尝一口，唇齿留香'},
      {id:'0008',name:'新绿豆乳茶',count:0,price:18,des:'醇香芝士与清新抹茶的完美碰撞，尝一口，唇齿留香'},
      {id:'0009',name:'醉意山谷茶',count:0,price:25,des:'醇香芝士与清新抹茶的完美碰撞，尝一口，唇齿留香'}
    ]
  },
  { title: '折扣' ,
    product:[
    {name:'抹茶芝士',count:20,price:15,des:'醇香芝士与清新抹茶的完美碰撞，尝一口，唇齿留香'},
    {name:'新绿豆乳茶',count:20,price:18,des:'醇香芝士与清新抹茶的完美碰撞，尝一口，唇齿留香'},
    {name:'醉意山谷茶',count:20,price:25,des:'醇香芝士与清新抹茶的完美碰撞，尝一口，唇齿留香'}
   ]
  },
  { title: '必点' ,
    product:[
    {name:'抹茶芝士',count:20,price:15,des:'醇香芝士与清新抹茶的完美碰撞，尝一口，唇齿留香'},
    {name:'新绿豆乳茶',count:20,price:18,des:'醇香芝士与清新抹茶的完美碰撞，尝一口，唇齿留香'},
    {name:'醉意山谷茶',count:20,price:25,des:'醇香芝士与清新抹茶的完美碰撞，尝一口，唇齿留香'}
   ]
  },
  { title: '套餐' ,
    product:[
    {name:'抹茶芝士',count:20,price:15,des:'醇香芝士与清新抹茶的完美碰撞，尝一口，唇齿留香'},
    {name:'新绿豆乳茶',count:20,price:18,des:'醇香芝士与清新抹茶的完美碰撞，尝一口，唇齿留香'},
    {name:'醉意山谷茶',count:20,price:25,des:'醇香芝士与清新抹茶的完美碰撞，尝一口，唇齿留香'}
   ]
  },
  { title: '单品' ,
    product:[
    {name:'抹茶芝士',count:20,price:15,des:'醇香芝士与清新抹茶的完美碰撞，尝一口，唇齿留香'},
    {name:'新绿豆乳茶',count:20,price:18,des:'醇香芝士与清新抹茶的完美碰撞，尝一口，唇齿留香'},
    {name:'醉意山谷茶',count:20,price:25,des:'醇香芝士与清新抹茶的完美碰撞，尝一口，唇齿留香'}
   ]
  },
  { title: '凑单' ,
    product:[
    {name:'抹茶芝士',count:20,price:15,des:'醇香芝士与清新抹茶的完美碰撞，尝一口，唇齿留香'},
    {name:'新绿豆乳茶',count:20,price:18,des:'醇香芝士与清新抹茶的完美碰撞，尝一口，唇齿留香'},
    {name:'醉意山谷茶',count:20,price:25,des:'醇香芝士与清新抹茶的完美碰撞，尝一口，唇齿留香'}
   ]
  },
]
