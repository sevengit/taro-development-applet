const config = {
  RET_CODE: {
    SUCCESS_CODE: 1,//后台返回成功的状态码
    ERROR_CODE: 0,//后台返回失败的状态码
  },
  //标的状态
  LOT_STATUS: {
      PREPARE: '未开始',
      PUBLICITY: '公示中',
      DURING: '进行中'
  }
}

export default config
