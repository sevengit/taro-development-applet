import Taro from '@tarojs/taro'

let token = ''

function http(method, url, data = {}) {
  return new Promise(function (resolve, reject) {
    Taro.request({
      url: url,
      method: method,
      data: JSON.stringify(data),
      header: {
        'Content-Type': 'application/json; charset=utf-8',
        'token': token
      },
      success: function (res) {
        if (res) {
          resolve(res);
          // if (res.data.code === 400) {
          //   console.log('400,参数错误')
          //   reject(res)
          // }
          // if (res.data.code === 401) {
          //   console.log('401,登录过期')
          //   Taro.showToast({
          //     title: '登录过期,重新登录',
          //     icon: 'none',
          //     duration: 2000
          //   })
          //   // Taro.navigateTo({
          //   //   url: '/pages/login/index'
          //   // })
          //   reject(res)
          // }
          // if (res.data.code === 404) {
          //   console.log('404,接口错误')
          //   reject(res)
          // }
          // if (res.data.code === 500) {
          //   console.log('500,后台程序错误')
          //   reject(res)
          // } else {
          //   resolve(res)
          //   console.log(res)
          // }
        } else {
          reject(res)
          console.log(res)
        }
      },
      fail: function (err) {
        console.log(err)
        reject(err)
      }
    })
  })
}
export default http
