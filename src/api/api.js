import Taro from '@tarojs/taro'
import http from './http.js'
// console.log('当前taro类型  weapp h5等',process.env.TARO_ENV)
// console.log('当前taro环境',process.env.BASE_URL)

let BASE_URL = ''
if (process.env.NODE_ENV === '"development"') {
  BASE_URL = 'http://rap2api.taobao.org/app/mock/274123'
} else if (process.env.NODE_ENV === '"production"') {
  BASE_URL = 'http://**********'
}

const api = {
  goods: {
    //标的列表
    testDataApi(params) {
      return http('post', BASE_URL + '/lotInfo/queryPageLotInfo', params)
    }
  },
  org: {
    getMockData(id) {
      return http('get', `http://rap2api.taobao.org/app/mock/274123/weixin/prodemo/index${id}`)
    }
  }
}
export default api
